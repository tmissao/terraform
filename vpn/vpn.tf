data "aws_availability_zones" "azs" {}

resource "aws_vpc" "custom_vpc" {
  cidr_block           = "${var.vpc_cidr_block}"
  instance_tenancy     = "default"
  enable_dns_support   = "true"
  enable_dns_hostnames = "true"
  enable_classiclink   = "false"

  tags {
    Name  = "CustomVPC"
    Owner = "Terraform"
  }
}

resource "aws_subnet" "public" {
  count                   = "${var.public_subnets}"
  vpc_id                  = "${aws_vpc.custom_vpc.id}"
  cidr_block              = "${element(var.public_subnets_cidr_block, count.index)}"
  availability_zone       = "${element(data.aws_availability_zones.azs.names, count.index)}"
  map_public_ip_on_launch = "true"

  tags {
    Name  = "public-${element(data.aws_availability_zones.azs.names, count.index)}"
    Owner = "Terraform"
  }
}

resource "aws_internet_gateway" "igw" {
  vpc_id = "${aws_vpc.custom_vpc.id}"

  tags {
    Name  = "IGW-CustomVPC"
    Owner = "Terraform"
  }
}

resource "aws_subnet" "private" {
  count                   = "${var.private_subnets}"
  vpc_id                  = "${aws_vpc.custom_vpc.id}"
  cidr_block              = "${element(var.private_subnets_cidr_block, count.index)}"
  availability_zone       = "${element(data.aws_availability_zones.azs.names, count.index)}"
  map_public_ip_on_launch = "false"

  tags {
    Name  = "private-${element(data.aws_availability_zones.azs.names, count.index)}"
    Owner = "Terraform"
  }
}

resource "aws_route_table" "public-route" {
  vpc_id = "${aws_vpc.custom_vpc.id}"

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = "${aws_internet_gateway.igw.id}"
  }

  tags {
    Name  = "PublicRoute"
    Owner = "Terraform"
  }
}

resource "aws_route_table_association" "public-association" {
  count          = "${aws_subnet.public.count}"
  subnet_id      = "${element(aws_subnet.public.*.id, count.index)}"
  route_table_id = "${aws_route_table.public-route.id}"
}

resource "aws_eip" "nat-ips" {
  vpc        = true
  depends_on = ["aws_internet_gateway.igw"]
}

resource "aws_nat_gateway" "nat-gw" {
  allocation_id = "${aws_eip.nat-ips.id}"
  subnet_id     = "${aws_subnet.public.0.id}"
  depends_on    = ["aws_internet_gateway.igw"]

  tags {
    Name  = "IGW-NAT-CustomVPC"
    Owner = "Terraform"
  }
}

resource "aws_route_table" "private-route" {
  vpc_id = "${aws_vpc.custom_vpc.id}"

  route {
    cidr_block     = "0.0.0.0/0"
    nat_gateway_id = "${aws_nat_gateway.nat-gw.id}"
  }

  tags {
    Name  = "PrivateRoute"
    Owner = "Terraform"
  }
}

resource "aws_route_table_association" "private-association" {
  count          = "${var.private_subnets}"
  subnet_id      = "${element(aws_subnet.private.*.id, count.index)}"
  route_table_id = "${aws_route_table.private-route.id}"
}
