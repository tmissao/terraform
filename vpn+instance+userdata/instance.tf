resource "aws_key_pair" "terraform-key" {
  key_name   = "terraform-key"
  public_key = "${file("${var.ssh_key_public_path}")}"
}

resource "aws_security_group" "ssh-world-wide" {
  name        = "SSH World Wide"
  description = "Allow SSH From all IPs"
  vpc_id      = "${aws_vpc.custom_vpc.id}"

  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags {
    Name  = "WideOpen"
    Owner = "Terraform"
  }
}

data "aws_ami" "amazon-linux-2" {
  most_recent = true
  owners      = ["amazon"]

  filter {
    name   = "owner-alias"
    values = ["amazon"]
  }

  filter {
    name = "virtualization-type"
    values = ["hvm"]
  }

  filter {
    name = "architecture"
    values = ["x86_64"]
  }

  filter {
    name   = "name"
    values = ["amzn2-ami-hvm*"]
  }
}

resource "aws_instance" "server" {
  count                  = "${var.public_servers}"
  instance_type          = "t2.micro"
  key_name               = "${aws_key_pair.terraform-key.key_name}"
  vpc_security_group_ids = ["${aws_security_group.ssh-world-wide.id}"]
  subnet_id              = "${element(aws_subnet.public.*.id, count.index)}"
  ami                    = "${data.aws_ami.amazon-linux-2.id}"

  # executes a start-up script from a file
  # user_data              = "${file("scripts/install_docker.sh")}"

  tags {
    Name  = "Public-Server-${count.index}"
    Owner = "Terraform"
  }
}

resource "aws_ebs_volume" "server-volume" {
  count             = "${var.public_servers}"
  availability_zone = "${element(data.aws_availability_zones.azs.names, count.index)}"
  size              = "${var.public_server_ebs_size}"
  encrypted         = "true"
  type              = "gp2"

  tags = {
    Name  = "EBS-Public-Server-${count.index}"
    Owner = "Terraform"
  }
}

resource "aws_volume_attachment" "server-volume-attachment" {
  count       = "${var.public_servers}"
  device_name = "/dev/sdf"
  volume_id   = "${element(aws_ebs_volume.server-volume.*.id, count.index)}"
  instance_id = "${element(aws_instance.server.*.id, count.index)}"
}

output "server" {
  value = ["${aws_instance.server.*.public_ip}"]
}

output "ami" {
  value = "${data.aws_ami.amazon-linux-2.id}"
}
