#! /bin/bash

# Docker
yum update -y
yum install -y docker
usermod -a -G docker ec2-user
curl -L https://github.com/docker/compose/releases/download/1.21.0/docker-compose-$(uname -s)-$(uname -m) -o /usr/local/bin/docker-compose
chmod +x /usr/local/bin/docker-compose
ln -s /usr/local/bin/docker-compose /usr/bin/docker-compose
service docker start
chkconfig docker on

# HTOP
yum install htop -y

# GIT
yum install -y git

# Mount Volume
mkfs.ext4 /dev/sdf
mkdir -p /data
echo '/dev/sdf /data ext4 defaults 0 0' >> /etc/fstab
mount /data