variable "aws_access_key" {}
variable "aws_secret_key" {}
variable "aws_region" {
    default = "us-west-2"
}

variable "vpc_cidr_block" { default = "10.0.0.0/16" }
variable "public_subnets_cidr_block" { default = ["10.0.1.0/24", "10.0.2.0/24", "10.0.3.0/24"] }
variable "private_subnets_cidr_block" { default = ["10.0.4.0/24", "10.0.5.0/24", "10.0.6.0/24"] }
variable "availability_zone" { default = ["us-west-2a", "us-west-2b", "us-west-2c", "us-west-2d"] }
variable "public_subnets" { default = 3 }
variable "private_subnets" { default = 3 }
variable "nat_gateways" { default = 2 }
variable "ssh_key_public_path" { default = "terraform-key.pub" }
variable "ssh_key_private_path" { default = "terraform-key" }
variable "public_servers" { default = 1 }
variable "public_server_ebs_size" { default = 20 }